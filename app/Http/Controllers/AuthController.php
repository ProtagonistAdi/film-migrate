<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata()
    {
        return view('halaman.biodata');
    }

    public function send(Request $request)
    {
        //dd($request->all());
        $firstname=$request['firstname'];
        $lastname=$request['lastname'];
        $gender=$request['gender'];
        $nationality=$request['nationality'];
        $language=$request['language'];
        $bio=$request['bio'];

        return view('halaman.home', compact('firstname','lastname','gender','nationality','language','bio'));

    }
}